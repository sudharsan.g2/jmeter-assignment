/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 99.0909090909091, "KoPercent": 0.9090909090909091};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.608457711442786, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.9333333333333333, 500, 1500, "https://demowebshop.tricentis.com/logout-23"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-22"], "isController": false}, {"data": [0.8, 500, 1500, "https://demowebshop.tricentis.com/logout-21"], "isController": false}, {"data": [0.9333333333333333, 500, 1500, "https://demowebshop.tricentis.com/logout-20"], "isController": false}, {"data": [0.2, 500, 1500, "https://demowebshop.tricentis.com/login-18"], "isController": false}, {"data": [0.9333333333333333, 500, 1500, "https://demowebshop.tricentis.com/login-19"], "isController": false}, {"data": [0.0, 500, 1500, "https://demowebshop.tricentis.com/login"], "isController": false}, {"data": [0.0, 500, 1500, "https://demowebshop.tricentis.com/logout"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-23"], "isController": false}, {"data": [0.8, 500, 1500, "https://demowebshop.tricentis.com/login-24"], "isController": false}, {"data": [0.0, 500, 1500, "Test"], "isController": true}, {"data": [0.9333333333333333, 500, 1500, "https://demowebshop.tricentis.com/login-21"], "isController": false}, {"data": [0.9333333333333333, 500, 1500, "https://demowebshop.tricentis.com/login-22"], "isController": false}, {"data": [0.9333333333333333, 500, 1500, "https://demowebshop.tricentis.com/logout-24"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-20"], "isController": false}, {"data": [0.55, 500, 1500, "https://demowebshop.tricentis.com/login-2"], "isController": false}, {"data": [0.48333333333333334, 500, 1500, "https://demowebshop.tricentis.com/login-1"], "isController": false}, {"data": [0.4, 500, 1500, "https://demowebshop.tricentis.com/login-4"], "isController": false}, {"data": [0.4666666666666667, 500, 1500, "https://demowebshop.tricentis.com/login-3"], "isController": false}, {"data": [0.43333333333333335, 500, 1500, "https://demowebshop.tricentis.com/login-6"], "isController": false}, {"data": [0.36666666666666664, 500, 1500, "https://demowebshop.tricentis.com/login-5"], "isController": false}, {"data": [0.5166666666666667, 500, 1500, "https://demowebshop.tricentis.com/login-8"], "isController": false}, {"data": [0.5333333333333333, 500, 1500, "https://demowebshop.tricentis.com/login-7"], "isController": false}, {"data": [0.4166666666666667, 500, 1500, "https://demowebshop.tricentis.com/login-0"], "isController": false}, {"data": [0.7, 500, 1500, "https://demowebshop.tricentis.com/logout-5"], "isController": false}, {"data": [0.7, 500, 1500, "https://demowebshop.tricentis.com/logout-4"], "isController": false}, {"data": [0.7666666666666667, 500, 1500, "https://demowebshop.tricentis.com/logout-3"], "isController": false}, {"data": [0.7666666666666667, 500, 1500, "https://demowebshop.tricentis.com/logout-2"], "isController": false}, {"data": [0.4666666666666667, 500, 1500, "https://demowebshop.tricentis.com/logout-1"], "isController": false}, {"data": [0.5666666666666667, 500, 1500, "https://demowebshop.tricentis.com/logout-0"], "isController": false}, {"data": [0.6, 500, 1500, "https://demowebshop.tricentis.com/logout-9"], "isController": false}, {"data": [0.4666666666666667, 500, 1500, "https://demowebshop.tricentis.com/logout-8"], "isController": false}, {"data": [0.7333333333333333, 500, 1500, "https://demowebshop.tricentis.com/logout-7"], "isController": false}, {"data": [0.7666666666666667, 500, 1500, "https://demowebshop.tricentis.com/logout-6"], "isController": false}, {"data": [0.6, 500, 1500, "https://demowebshop.tricentis.com/login-9"], "isController": false}, {"data": [0.6333333333333333, 500, 1500, "https://demowebshop.tricentis.com/logout-12"], "isController": false}, {"data": [0.6, 500, 1500, "https://demowebshop.tricentis.com/logout-11"], "isController": false}, {"data": [0.5333333333333333, 500, 1500, "https://demowebshop.tricentis.com/logout-10"], "isController": false}, {"data": [0.8, 500, 1500, "https://demowebshop.tricentis.com/login-16"], "isController": false}, {"data": [0.4, 500, 1500, "https://demowebshop.tricentis.com/login-17"], "isController": false}, {"data": [0.8666666666666667, 500, 1500, "https://demowebshop.tricentis.com/logout-19"], "isController": false}, {"data": [0.8333333333333334, 500, 1500, "https://demowebshop.tricentis.com/login-14"], "isController": false}, {"data": [0.9333333333333333, 500, 1500, "https://demowebshop.tricentis.com/logout-18"], "isController": false}, {"data": [0.9666666666666667, 500, 1500, "https://demowebshop.tricentis.com/login-15"], "isController": false}, {"data": [0.9666666666666667, 500, 1500, "https://demowebshop.tricentis.com/logout-17"], "isController": false}, {"data": [0.5833333333333334, 500, 1500, "https://demowebshop.tricentis.com/login-12"], "isController": false}, {"data": [0.8, 500, 1500, "https://demowebshop.tricentis.com/logout-16"], "isController": false}, {"data": [0.5666666666666667, 500, 1500, "https://demowebshop.tricentis.com/login-13"], "isController": false}, {"data": [0.9, 500, 1500, "https://demowebshop.tricentis.com/logout-15"], "isController": false}, {"data": [0.6, 500, 1500, "https://demowebshop.tricentis.com/login-10"], "isController": false}, {"data": [0.8, 500, 1500, "https://demowebshop.tricentis.com/logout-14"], "isController": false}, {"data": [0.5166666666666667, 500, 1500, "https://demowebshop.tricentis.com/login-11"], "isController": false}, {"data": [0.3, 500, 1500, "https://demowebshop.tricentis.com/logout-13"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 990, 9, 0.9090909090909091, 1068.2454545454525, 180, 8389, 750.0, 2707.2999999999997, 3610.0999999999976, 6778.27, 29.851646363526715, 1042.1281123733565, 54.589148813472434], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["https://demowebshop.tricentis.com/logout-23", 15, 0, 0.0, 312.6666666666667, 196, 1018, 219.0, 894.4000000000001, 1018.0, 1018.0, 0.8965393580778196, 0.10856531289223596, 0.8037335260892953], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-22", 15, 0, 0.0, 230.2, 202, 301, 220.0, 297.4, 301.0, 301.0, 0.8840690752637473, 0.10705523958271941, 0.783920625331526], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-21", 15, 1, 6.666666666666667, 439.0, 180, 1324, 226.0, 1167.4, 1324.0, 1324.0, 0.8822491471591578, 0.28276774423597223, 0.7341737369133043], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-20", 15, 0, 0.0, 313.6, 196, 861, 221.0, 846.0, 861.0, 861.0, 0.8852691218130312, 0.10806507834631729, 0.7780685640934845], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-18", 15, 0, 0.0, 1676.533333333333, 533, 2831, 1631.0, 2637.8, 2831.0, 2831.0, 1.0049577917727455, 432.5509100102171, 1.148242789427844], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-19", 15, 0, 0.0, 366.46666666666664, 205, 1166, 247.0, 1147.4, 1166.0, 1166.0, 1.0854620450104928, 3.250026005861495, 1.267785747883349], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login", 30, 0, 0.0, 5624.033333333332, 3043, 8389, 5546.5, 7181.2, 7877.499999999999, 8389.0, 1.2410540685889215, 690.645377202871, 24.078751667666406], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout", 15, 4, 26.666666666666668, 4554.2, 2968, 7738, 4279.0, 6750.400000000001, 7738.0, 7738.0, 0.7299625285901991, 28.49762305951628, 15.725417371283275], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-23", 15, 0, 0.0, 264.4666666666667, 195, 333, 264.0, 325.8, 333.0, 333.0, 1.0833453705041167, 4.16834058572873, 1.2790669462299582], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-24", 15, 0, 0.0, 592.6, 204, 2406, 284.0, 2090.4, 2406.0, 2406.0, 1.0910677916787896, 4.199119303716905, 1.2690056053607799], "isController": false}, {"data": ["Test", 15, 4, 26.666666666666668, 15802.266666666668, 12600, 20555, 15598.0, 20190.2, 20555.0, 20555.0, 0.7048872180451128, 812.0567838052161, 42.53746548989661], "isController": true}, {"data": ["https://demowebshop.tricentis.com/login-21", 15, 0, 0.0, 399.3333333333333, 204, 2512, 246.0, 1187.2000000000007, 2512.0, 2512.0, 1.0905918278319033, 4.197287493638214, 1.2833624536498474], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-22", 15, 0, 0.0, 379.66666666666663, 196, 1309, 263.0, 1160.2, 1309.0, 1309.0, 1.085304970696766, 4.191778475689169, 1.2707818943998264], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-24", 15, 0, 0.0, 296.5333333333333, 194, 939, 226.0, 757.8000000000001, 939.0, 939.0, 0.8975586404978458, 0.1095652637326472, 0.789746420984921], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-20", 15, 0, 0.0, 262.73333333333335, 212, 383, 257.0, 375.8, 383.0, 383.0, 1.0785159620362381, 8.021462467644522, 1.2533535105694564], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-2", 30, 0, 0.0, 1324.7333333333336, 191, 3588, 847.5, 3331.6, 3460.3999999999996, 3588.0, 1.5060240963855422, 18.869423004518072, 1.4846985598644578], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-1", 30, 0, 0.0, 1326.3999999999999, 399, 3938, 814.0, 3401.2, 3866.5, 3938.0, 1.5926948396687195, 100.3926573449777, 1.471376287428329], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-4", 30, 0, 0.0, 1936.5999999999997, 219, 6075, 1045.0, 3797.0, 4830.899999999999, 6075.0, 1.5462323471807031, 71.40830157651273, 1.5349074032316254], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-3", 30, 0, 0.0, 1538.0000000000002, 203, 3487, 1222.0, 3353.4, 3429.7999999999997, 3487.0, 1.5676438313215237, 20.990199776610755, 1.5676438313215237], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-6", 30, 0, 0.0, 1371.2666666666669, 210, 3020, 1180.0, 2941.5, 2984.8, 3020.0, 1.5300657928290915, 6.263706839394094, 1.4702975977967052], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-5", 30, 0, 0.0, 1688.833333333333, 223, 3544, 1525.5, 3378.3, 3531.9, 3544.0, 1.5060997038003916, 18.038632634042873, 1.4303534784627743], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-8", 30, 0, 0.0, 869.6333333333334, 249, 2073, 841.0, 1292.5000000000005, 1912.9499999999998, 2073.0, 1.5714210884710074, 5.846024059111623, 1.537660088523388], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-7", 30, 0, 0.0, 1261.6999999999996, 210, 4469, 1161.5, 3772.9000000000037, 4307.3, 4469.0, 1.5212210334161553, 169.77361537193855, 1.488538550276355], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-0", 30, 0, 0.0, 1408.0333333333335, 245, 2691, 1892.0, 2519.0, 2614.0, 2691.0, 1.4929829799940282, 17.51776416467602, 0.9899760189608838], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-5", 15, 0, 0.0, 674.3333333333334, 234, 1307, 833.0, 1145.0, 1307.0, 1307.0, 0.8335648791330925, 0.10093949708252292, 0.7383235794665184], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-4", 15, 0, 0.0, 627.9333333333333, 219, 1209, 831.0, 1030.2, 1209.0, 1209.0, 0.8665511265164644, 0.10408768414211439, 0.7717720970537262], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-3", 15, 0, 0.0, 544.9999999999999, 198, 1221, 479.0, 1006.2000000000002, 1221.0, 1221.0, 0.8362136247073252, 0.10126024361690267, 0.7251540026758836], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-2", 15, 0, 0.0, 533.1333333333334, 212, 929, 459.0, 880.4, 929.0, 929.0, 0.8392077878482712, 0.1016228180597516, 0.7244723481033905], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-1", 15, 0, 0.0, 937.8666666666667, 511, 2253, 865.0, 1782.0000000000002, 2253.0, 2253.0, 0.8529512111907199, 29.391832459058346, 0.633882687222791], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-0", 15, 0, 0.0, 928.4000000000001, 268, 3071, 820.0, 2202.2000000000007, 3071.0, 3071.0, 0.8372404554588078, 0.5649737839082384, 0.8944736897186872], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-9", 15, 0, 0.0, 840.5999999999999, 216, 1321, 864.0, 1298.2, 1321.0, 1321.0, 0.8266740148801323, 0.09929775764673464, 0.7160740734362083], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-8", 15, 0, 0.0, 1106.8666666666668, 259, 2796, 866.0, 2277.6000000000004, 2796.0, 2796.0, 0.8357942831671031, 0.1012094639772664, 0.7149958906781078], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-7", 15, 0, 0.0, 654.8666666666669, 198, 1429, 813.0, 1322.2, 1429.0, 1429.0, 0.8555783709787816, 0.10276966760780287, 0.7294139822895277], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-6", 15, 0, 0.0, 531.6666666666666, 204, 1065, 446.0, 1030.2, 1065.0, 1065.0, 0.8341675008341675, 0.10101247080413747, 0.7478181306306306], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-9", 30, 0, 0.0, 768.7, 249, 2122, 708.5, 1198.3000000000002, 1688.0499999999995, 2122.0, 1.5786150284150704, 2.7271191262365817, 1.5446994711639654], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-12", 15, 0, 0.0, 773.1333333333333, 215, 1332, 848.0, 1210.8000000000002, 1332.0, 1332.0, 0.8158381377134776, 0.09879289948874144, 0.6931437302839117], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-11", 15, 0, 0.0, 863.7333333333335, 201, 1420, 874.0, 1419.4, 1420.0, 1420.0, 0.8113809704116406, 0.09825316438578462, 0.6980728856764213], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-10", 15, 1, 6.666666666666667, 829.8, 195, 1428, 863.0, 1336.8, 1428.0, 1428.0, 0.8140229011776198, 0.2654583796874152, 0.6558819938134259], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-16", 15, 0, 0.0, 583.1999999999999, 203, 2347, 260.0, 1679.2000000000003, 2347.0, 2347.0, 1.0898786601758337, 0.12984882474751144, 1.3442546365254668], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-17", 15, 0, 0.0, 1248.7333333333333, 223, 2844, 865.0, 2735.4, 2844.0, 2844.0, 1.0538888498559686, 57.378278582343846, 1.204150346026839], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-19", 15, 0, 0.0, 418.3333333333333, 197, 1092, 228.0, 1033.8, 1092.0, 1092.0, 0.8790951180917775, 0.10645292445642619, 0.7769346502666589], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-14", 15, 0, 0.0, 580.9333333333333, 329, 1549, 448.0, 1342.6000000000001, 1549.0, 1549.0, 1.0841283607979184, 31.576297227341716, 1.2630518890936686], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-18", 15, 0, 0.0, 329.1333333333333, 196, 1179, 223.0, 958.2000000000002, 1179.0, 1179.0, 0.8810055209679314, 0.1703506769059086, 0.7571141195818161], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-15", 15, 0, 0.0, 290.26666666666665, 201, 939, 233.0, 609.6000000000001, 939.0, 939.0, 1.1002714002787355, 0.13216150608816843, 1.3237640284603536], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-17", 15, 0, 0.0, 278.66666666666663, 196, 934, 217.0, 571.0000000000002, 934.0, 934.0, 0.8757078638566174, 0.10604274913888725, 0.7517062620409831], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-12", 30, 0, 0.0, 800.0, 242, 1859, 833.5, 1073.9, 1440.4499999999994, 1859.0, 1.5873015873015872, 5.882626488095238, 1.5547495039682542], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-16", 15, 0, 0.0, 489.06666666666666, 200, 1043, 222.0, 1015.4, 1043.0, 1043.0, 0.8855827134254339, 0.1055088779667021, 0.767101432430039], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-13", 15, 0, 0.0, 864.6666666666667, 211, 1550, 937.0, 1428.2, 1550.0, 1550.0, 1.0565612453335211, 0.12794296330210608, 1.287684017750229], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-15", 15, 0, 0.0, 391.6666666666667, 200, 1444, 224.0, 1101.4, 1444.0, 1444.0, 0.8881520516312392, 0.10668232651429925, 0.742439605660489], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-10", 30, 0, 0.0, 778.9666666666666, 209, 2708, 723.0, 1112.0000000000002, 1909.399999999999, 2708.0, 1.5894881848044928, 3.099812407279856, 1.5545629073063474], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-14", 15, 1, 6.666666666666667, 552.5333333333333, 203, 3410, 227.0, 2010.2000000000007, 3410.0, 3410.0, 0.8931761343336906, 0.320810724812433, 0.7343117333571514], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-11", 30, 0, 0.0, 953.4666666666666, 243, 2790, 846.5, 1608.8000000000002, 2740.5, 2790.0, 1.582528881152081, 1.2023510444690615, 1.5338475727963286], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-13", 15, 2, 13.333333333333334, 1240.933333333333, 455, 3072, 911.0, 2650.8, 3072.0, 3072.0, 0.860634574559642, 0.45710005594124736, 0.6351662459119858], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["Non HTTP response code: java.net.SocketException/Non HTTP response message: Socket closed", 5, 55.55555555555556, 0.5050505050505051], "isController": false}, {"data": ["Assertion failed", 4, 44.44444444444444, 0.40404040404040403], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 990, 9, "Non HTTP response code: java.net.SocketException/Non HTTP response message: Socket closed", 5, "Assertion failed", 4, "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-21", 15, 1, "Non HTTP response code: java.net.SocketException/Non HTTP response message: Socket closed", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout", 15, 4, "Assertion failed", 4, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-10", 15, 1, "Non HTTP response code: java.net.SocketException/Non HTTP response message: Socket closed", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-14", 15, 1, "Non HTTP response code: java.net.SocketException/Non HTTP response message: Socket closed", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-13", 15, 2, "Non HTTP response code: java.net.SocketException/Non HTTP response message: Socket closed", 2, "", "", "", "", "", "", "", ""], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
